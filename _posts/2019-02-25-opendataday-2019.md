---
layout: post
title:  Open Data Day 2019
author: germaaan
image:
  feature: banners/header.jpg
tags: eventos opendata
---

<p style="text-align:center;"><img src="/assets/images/opendataday_2019.png" alt="opendataday_2019"></p>

El OpenDataDay se celebra todos los años a principios de marzo para tratar con datos y ayudar a la ciudadanía a tomar control de todo lo que le afecta. En años anteriores se han tratado datos del Ayuntamiento de Granada y la contaminación atmosférica. Este año nos centraremos en estos temas:

-   Datos abiertos en la universidad.
-   Cambio climático y contaminación atmosférica.
-   Salud y nutrición.
-   Aprendizaje automático y machine learning, centrado en trabajar en la biblioteca [Shogun](http://shogun-toolbox.org/), organización hermana de [PyData](https://pydata.org/).

Este hackatón sirve también como lanzamiento para **PyData Granada**, el nuevo capítulo de la organización PyData en Granada.

<p style="text-align:center;"><img src="/assets/images/pydata.png" alt="pydata_granada"></p>

La organización del mismo será como sigue:

<div class="bootstrap">
<table id="tablePreview" class="table table-striped table-hover table-sm">
  <tbody>
  <tr>
<th colspan="3">Viernes, 8 de marzo</th>
</tr>
<tr>
<td>10:00</td>
<td>10:30</td>
<td>Presentación de Open Data Day / PyData Granada.</td>
</tr>
<tr>
<td>10:30</td>
<td>11:30</td>
<td>Micrófono abierto para presentación de diferentes proyectos. Cualquier <br>persona puede presentar su propio proyecto, centrado en las temáticas <br>generales del ODD.</td>
</tr>
<tr>
<td>11:30</td>
<td>12:00</td>
<td>Pausa para café.<br></td>
</tr>
<tr>
<td>12:00</td>
<td>14:00</td>
<td>Organización de grupos de trabajo a lo largo de una temática. Mini-talleres a demanda para organizar los mismos.</td>
</tr>
<tr>
<td>14:00</td>
<td>15:30<br></td>
<td>Pausa para comer.</td>
</tr>
<tr>
<td>15:30</td>
<td>19:00</td>
<td>Trabajo en grupo.</td>
</tr>
<tr>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<th colspan="3">Sábado, 9 de marzo</th>
</tr>
<tr>
<td>10:00</td>
<td>18:00</td>
<td>Trabajo en grupo y preparación de la presentación (cada grupo se irá organizando sus propias pausas).</td>
</tr>
<tr>
<td>18:00</td>
<td>19:00</td>
<td>Presentación del trabajo por parte de cada uno de los grupos.</td>
</tr>
<tr>
<td>19:00</td>
<td>Cierre</td>
<td>Data&amp;Beers en La Posada</td>
</tr>
  </tbody>
</table>
</div>

**PyData** y el **Open Data Day** buscan la creación de un entorno abierto y acogedor para todo tipo de participantes, por lo que los participantes en el hackatón se comprometen a seguir el [código de conducta de PyData](https://pydata.org/code-of-conduct/).

Este evento cuenta con el apoyo y la colaboración de **Interferencias** y de la **[Escuela Técnica Superior de Ingenierías Informática y Telecomunicación](https://etsiit.ugr.es/)**.

Registro no obligatorio: [https://www.meetup.com/es-ES/Granada-Geek/events/259269663/](https://www.meetup.com/es-ES/Granada-Geek/events/259269663/).
