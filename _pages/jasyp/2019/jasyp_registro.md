---
layout: page
title: JASYP '19 - Participación
permalink: /jasyp/19/registro
description: "Formulario de participación para las JASYP '19"
image:
  feature: banners/header.jpg
timing: false
---

Las **JASYP** (_Jornadas sobre Anonimato, Seguridad y Privacidad_) son unas jornadas que, desde hace ya unos cuantos años, son organizadas por [Interferencias](https://twitter.com/Inter_ferencias) con el apoyo de integrantes de otros grupos del ámbito de la seguridad informática como [Hacking Desde Cero](https://www.hackingdesdecero.org/), [Hack&Beers](https://twitter.com/hackandbeers), [BitUp Alicante](https://twitter.com/bitupalicante/), [HackMadrid%27](http://hackmadrid.org/) o [Follow the White Rabbit](https://twitter.com/fwhibbit_blog), con el objetivo de dar un espacio a todas aquellas personas con inquietudes sobre los problemas que encontramos en el día a día cada vez que usamos Internet o cualquier dispositivo electrónico en general.

![mosaico_v_2018]({{ "/assets/images/jasyp/18/charlas/logo.png" }})

En este 2019 lanzamos la tercera edición de este evento, donde esperamos poder aprender todavía más que otros años, aprovechar los días al máximo, y sacar partido del buen rollo que Granada siempre brinda en sus bares. La **edición de este año** tendrá lugar **los días VIERNES 26 Y SÁBADO 27 DE ABRIL** en la **[Escuela Técnica Superior de Ingenierías Informática y de Telecomunicación de la Universidad de Granada](https://etsiit.ugr.es/)** _(Calle Periodista Daniel Saucedo Aranda, s/n, 18071 Granada)_; pero la cosa no termina ahí porque seguramente tengamos un par de sorpresas que os iremos desvelando según se vayan acercando las fechas.

Volviendo al tema de las jornadas, queremos avisar que desde ya el **Call for Papers** se encuentra abierto para todas aquellas personas interesadas en participar (puedes encontrar [el formulario](#form) al final de esta misma página). En la edición del año pasado nos hizo muy felices ver la respuesta tan buena en cuanto a participaciones recibidas, consiguiendo un equilibrio entre personas con experiencia y personas que están empezando, equilibrio entre personas con perfil técnico y personas con un perfil más social, equilibrio entre personas de diferentes edades y de diferentes sexos. Por ello solo nos queda más que daros las gracias y esperar que este año la cosa vaya todavía mejor.

<div class="bootstrap">
	<div class="text-center">
    <br>
		<h3>¿Qué pedimos?</h3>
		<hr>
  </div>
</div>

- Personas interesadas en hablar sobre la importancia de los **derechos digitales**, la **privacidad en Internet**, la **seguridad informática** y todos aquellos temas de este ámbito que puedan tener relación.
- Las charlas pueden ser de cualquier área. Es cierto que la temática se presta a talleres y charlas técnicas, pero nos encantaría acoger también charlas relacionadas con la temática desde un punto de vista distinto, como puede ser el de personas dedicadas a **derecho, filosofía, política, arte** o cualquier otra rama. ¡Lánzate y cuéntanoslo!
- Si la charla está apoyada en Software Libre o cualquier contenido que sea libre, muchísimo mejor.
- Como complemento a la actividad principal, prepararemos otra edición del [**Capture The Flag**]({{ site.url }}/jasyp/ctf/), donde cualquiera que se anime pueda poner a prueba sus habilidades a la hora de explotar las debilidades de sistemas informáticos (las bases de participación serán publicadas próximamente).
- Además, este año también vamos a organizar un concurso artístico como el que realizamos [el año pasado]({{ site.url }}/jasyp/arte/).

![sabado]({{ "/assets/images/jasyp/18/charlas/sabado.jpeg" }})

<div class="bootstrap">
	<div class="text-center">
    <br>
		<h3>¿Qué ofrecemos?</h3>
		<hr>
  </div>
</div>

Los integrantes de Interferencias somos un grupo de personas con mucha energía e interés, pero por desgracia no contamos con la suficiente solvencia económica como para poder hacernos cargo de los gastos de viaje y alojamiento para los participantes que tengan que desplazarse. Sin embargo, podemos ofreceros cerveza y tapas gratis, además de mucho buen humor granadino.

Es por eso que también estamos buscando patrocinadores que nos ayuden a cubrir estos gastos que nos gustaría poder afrontar de cara a hacer un evento de una mayor magnitud. Puedes encontrar más información sobre cómo patrocinar las JASYP en [esta página]({{ site.url }}/jasyp/patrocinio/) o escribiendo al correo [jasyp@interferencias.tech](mailto:[jasyp@interferencias.tech]).

<div class="bootstrap">
	<div class="text-center">
    <br>
		<h3>¿Te hemos convencido?</h3>
		<hr>
  </div>
</div>

En el caso de sea así, si todo esto te suena bien, aquí te dejamos el formulario a rellenar; aunque también agradecemos enormemente que nos ayudes con la difusión del evento a través de redes sociales o el simple boca a boca. Aceptamos propuestas de participantes de todas las edades, géneros, etnias y condición social, **seas quien seas, si te gusta la temática ¡Anímate!**

![mosaico_v_2018]({{ "/assets/images/jasyp/18/charlas/mosaico_v.jpg" }})

<div id="form" class="bootstrap">
	<div class="text-center">
    <br>
		<h3>Registrar participación</h3>
		<hr>

		<div class="form-group">
			<form name="reg_form" role="form" class="form-horizontal" method="post" action="{{ site.url }}/jasyp_app/form">
				<input type="hidden" name="good" value="{{ site.url }}/jasyp/success/" />
				<input type="hidden" name="bad" value="{{ site.url }}/jasyp/error/" />
				<label for="name" style="margin: 0 auto;">Nombre:</label>
				<input type="text" name="name" class="form-control" style="width: 250px; margin: 0 auto;" required minlength="2" maxlength="255" pattern="[a-zA-ZÁáÉéÍíÓóÚúñÑÇç ]+" oninvalid="this.setCustomValidity('Introduce una cadena con entre 2 y 255 caracteres alfabéticos')"
				 oninput="setCustomValidity('')" />
				<label for="email" style="margin: 0 auto;">Email:</label>
				<input type="email" name="email" class="form-control" style="width: 250px; margin: 0 auto;" required maxlength="255" oninvalid="this.setCustomValidity('Introduce una dirección de correo eletrónico válida')" oninput="setCustomValidity('')" />
				<label for="title" style="margin: 0 auto;">Título:</label>
				<input type="text" name="title" class="form-control" style="width: 350px; margin: 0 auto;" required minlength="20" maxlength="255" pattern="[a-zA-Z0-9ÁáÉéÍíÓóÚúñÑÇç.,-:¿?¡! ]+" oninvalid="this.setCustomValidity('Introduce una cadena con entre 20 y 255 caracteres alfanuméricos o signos de puntuación/exclamación')"
				 oninput="setCustomValidity('')" />
				<label for="type">Tipo:</label>
				<p>
					<input type="radio" name="type" value="T" required oninvalid="this.setCustomValidity('Selecciona una de las dos opciones')" oninput="setCustomValidity('')" /> Charla &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" name="type" value="W" /> Taller
				</p>
				<label for="length">Duración:</label>
				<p>
					<input type="radio" name="length" value="S" required oninvalid="this.setCustomValidity('Selecciona una de las dos opciones')" oninput="setCustomValidity('')" /> Corta (~20-30 min) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="radio" name="length" value="L" /> Larga (~45-60 min)
				</p>
				<label for="title">Resumen:</label>
				<textarea name="abstract" required class="form-control" style="resize:none; overflow-y: scroll;" rows="4" cols="50" required minlength="150" maxlength="2000" oninvalid="this.setCustomValidity('Introduce una cadena con entre 150 y 2000 caracteres alfanúmericos o signos de puntuación')"
				 oninput="setCustomValidity('')"></textarea>
				<div class="col-xs-12" style="height:12px;"></div>
				<button type="submit" class="btn btn-default">Enviar</button>
			</form>
		</div>
	</div>
</div>
